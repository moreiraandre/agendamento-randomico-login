# agendamento-randomico-login
Faça um agendemaneto para horarios randômicos dentro de um intervalo para fazer login em determinada página.

## Pré-requisitos
* Node
* Npm
> Para verificar se você já possui as ferramentas instaladas, execute `node -v ; npm -v`.
> Caso você não possua as ferramentas, já para https://github.com/nodesource/distributions#installation-instructions

## Baixando e Instalando
```sh
sudo apt install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
cd /meu/diretorio
git clone git@gitlab.com:moreiraandre/agendamento-randomico-login.git login-agendado ; cd $_
npm i
npm i -g node-notifier-cli
cp fixtures/env.example.json fixtures/env.json
```
> Edite as informações do arquivo `fixtures/env.json` conforme sua necessidade! Segue explicação do conteúdo.
```json
{
  "visit_url": "https://mysite.com.br", /* URL da página que será visitada */
  "selector_dom_input_login": "input[name=login]", /* Seletor HTML do campo de usuario */
  "selector_dom_input_password": "input[name=password]", /* Seletor HTML do campo de senha */
  "selector_dom_submit": "button[type=submit]", /* Seletor HTML do botão de envio */
  "login": "login@cypress.io", /* Seu login */
  "password": "*************", /* Sua senha */
  "see_text_after_login": "LOGIN SUCCESS" /* Algum texto que aparecerá na página caso o login seja bem sucedido */
}
```
Caso queira testar manualmente execute
```sh
# Para acompanhar somente no terminal
npm run cypress

# Para executar no navegador Google Chrome
npm run cypress:chrome
```

## Configurando CRON do Linux
Esta configuração faz com que a aplicação execute automaticamente em horários agendados.
```sh
crontab -e
```
Cole as seguintes linhas
```
30 7 * * 1-5 sleep $(shuf -i 0-30 -n 1)m && cd /meu/diretorio/login-agendado && npm run cypress >> /meu/diretorio/login-agendado/log.log
00 12 * * 1-5 sleep $(shuf -i 0-30 -n 1)m && cd /meu/diretorio/login-agendado && npm run cypress >> /meu/diretorio/login-agendado/log.log
30 13 * * 1-5 sleep $(shuf -i 0-30 -n 1)m && cd /meu/diretorio/login-agendado && npm run cypress >> /meu/diretorio/login-agendado/log.log
00 18 * * 1-5 sleep $(shuf -i 0-30 -n 1)m && cd /meu/diretorio/login-agendado && npm run cypress >> /meu/diretorio/login-agendado/log.log
```
* Os dois primeiros grupos de números determinam respectivamente **minuto** e **hora** do dia em que será solicita a geração de um valor em minutos que o sistema aguardará antes de executar a aplicação.
* `1-5` Garante que a aplicação seja executada de segunda a sexta-feira.
* `sleep $(shuf -i 0-30 -n 1)m` gera um valor randômico de minutos para que o sistema aguarde antes de executar a aplicação.
* A aplicação será executada em segundo plano, quando for finalizada uma notificação de desktop aparecerá com a mensagem `Login efetuado!`.

## Observações
* A pasta `cypress/screenshots` armazenará um screenshot da tela caso o fluxo da aplicação **não** seja executado com sucesso.
* A pasta `cypress/videos` armazenará uma gravação em vídeo caso o fluxo da aplicação seja **bem** sucedido.
* Esta aplicação foi desenvolvida com [Cypress](https://www.cypress.io/)
