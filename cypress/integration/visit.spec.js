describe('Visit Website', () => {
    it('Should visit website', () => {
        cy.fixture('env.json').then((dataEnv) => {
            cy.visit(dataEnv.visit_url)

            cy.get(dataEnv.selector_dom_input_login).type(dataEnv.login)
            cy.get(dataEnv.selector_dom_input_password).type(dataEnv.password)

            cy.get(dataEnv.selector_dom_submit).click()

            cy.contains(dataEnv.see_text_after_login).should('to.have.length', 1)

            cy.task('notifier', {
                title: 'Agendamento',
                message: 'Login efetuado!'
            })
        })
    })
})